// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use dazzle_sys;
use glib::StaticType;
use glib::Value;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect_raw;
use glib::translate::*;
use glib_sys;
use gobject_sys;
use gtk;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib_wrapper! {
    pub struct CenteringBin(Object<dazzle_sys::DzlCenteringBin, dazzle_sys::DzlCenteringBinClass, CenteringBinClass>) @extends gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        get_type => || dazzle_sys::dzl_centering_bin_get_type(),
    }
}

impl CenteringBin {
    pub fn new() -> CenteringBin {
        assert_initialized_main_thread!();
        unsafe {
            gtk::Widget::from_glib_none(dazzle_sys::dzl_centering_bin_new()).unsafe_cast()
        }
    }
}

impl Default for CenteringBin {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_CENTERING_BIN: Option<&CenteringBin> = None;

/// Trait containing all `CenteringBin` methods.
///
/// # Implementors
///
/// [`CenteringBin`](struct.CenteringBin.html)
pub trait CenteringBinExt: 'static {
    fn get_property_max_width_request(&self) -> i32;

    fn set_property_max_width_request(&self, max_width_request: i32);

    fn connect_property_max_width_request_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<CenteringBin>> CenteringBinExt for O {
    fn get_property_max_width_request(&self) -> i32 {
        unsafe {
            let mut value = Value::from_type(<i32 as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"max-width-request\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get().unwrap()
        }
    }

    fn set_property_max_width_request(&self, max_width_request: i32) {
        unsafe {
            gobject_sys::g_object_set_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"max-width-request\0".as_ptr() as *const _, Value::from(&max_width_request).to_glib_none().0);
        }
    }

    fn connect_property_max_width_request_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::max-width-request\0".as_ptr() as *const _,
                Some(transmute(notify_max_width_request_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }
}

unsafe extern "C" fn notify_max_width_request_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlCenteringBin, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<CenteringBin> {
    let f: &F = &*(f as *const F);
    f(&CenteringBin::from_glib_borrow(this).unsafe_cast())
}

impl fmt::Display for CenteringBin {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "CenteringBin")
    }
}
