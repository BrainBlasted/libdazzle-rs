// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use Dock;
use dazzle_sys;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect_raw;
use glib::translate::*;
use glib_sys;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib_wrapper! {
    pub struct DockManager(Object<dazzle_sys::DzlDockManager, dazzle_sys::DzlDockManagerClass, DockManagerClass>);

    match fn {
        get_type => || dazzle_sys::dzl_dock_manager_get_type(),
    }
}

impl DockManager {
    pub fn new() -> DockManager {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(dazzle_sys::dzl_dock_manager_new())
        }
    }
}

impl Default for DockManager {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_DOCK_MANAGER: Option<&DockManager> = None;

/// Trait containing all `DockManager` methods.
///
/// # Implementors
///
/// [`DockManager`](struct.DockManager.html)
pub trait DockManagerExt: 'static {
    /// Requests that the transient grab monitoring stop until
    /// `DockManagerExt::unpause_grabs` is called.
    ///
    /// This might be useful while setting up UI so that you don't focus
    /// something unexpectedly.
    ///
    /// This function may be called multiple times and after an equivalent
    /// number of calls to `DockManagerExt::unpause_grabs`, transient
    /// grab monitoring will continue.
    ///
    /// Feature: `v3_26`
    ///
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn pause_grabs(&self);

    fn register_dock<P: IsA<Dock>>(&self, dock: &P);

    fn release_transient_grab(&self);

    /// Unpauses a previous call to `DockManagerExt::pause_grabs`.
    ///
    /// Once the pause count returns to zero, transient grab monitoring
    /// will be restored.
    ///
    /// Feature: `v3_26`
    ///
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn unpause_grabs(&self);

    fn unregister_dock<P: IsA<Dock>>(&self, dock: &P);

    fn connect_register_dock<F: Fn(&Self, &Dock) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_unregister_dock<F: Fn(&Self, &Dock) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<DockManager>> DockManagerExt for O {
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn pause_grabs(&self) {
        unsafe {
            dazzle_sys::dzl_dock_manager_pause_grabs(self.as_ref().to_glib_none().0);
        }
    }

    fn register_dock<P: IsA<Dock>>(&self, dock: &P) {
        unsafe {
            dazzle_sys::dzl_dock_manager_register_dock(self.as_ref().to_glib_none().0, dock.as_ref().to_glib_none().0);
        }
    }

    fn release_transient_grab(&self) {
        unsafe {
            dazzle_sys::dzl_dock_manager_release_transient_grab(self.as_ref().to_glib_none().0);
        }
    }

    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn unpause_grabs(&self) {
        unsafe {
            dazzle_sys::dzl_dock_manager_unpause_grabs(self.as_ref().to_glib_none().0);
        }
    }

    fn unregister_dock<P: IsA<Dock>>(&self, dock: &P) {
        unsafe {
            dazzle_sys::dzl_dock_manager_unregister_dock(self.as_ref().to_glib_none().0, dock.as_ref().to_glib_none().0);
        }
    }

    fn connect_register_dock<F: Fn(&Self, &Dock) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"register-dock\0".as_ptr() as *const _,
                Some(transmute(register_dock_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_unregister_dock<F: Fn(&Self, &Dock) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"unregister-dock\0".as_ptr() as *const _,
                Some(transmute(unregister_dock_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }
}

unsafe extern "C" fn register_dock_trampoline<P, F: Fn(&P, &Dock) + 'static>(this: *mut dazzle_sys::DzlDockManager, object: *mut dazzle_sys::DzlDock, f: glib_sys::gpointer)
where P: IsA<DockManager> {
    let f: &F = &*(f as *const F);
    f(&DockManager::from_glib_borrow(this).unsafe_cast(), &from_glib_borrow(object))
}

unsafe extern "C" fn unregister_dock_trampoline<P, F: Fn(&P, &Dock) + 'static>(this: *mut dazzle_sys::DzlDockManager, object: *mut dazzle_sys::DzlDock, f: glib_sys::gpointer)
where P: IsA<DockManager> {
    let f: &F = &*(f as *const F);
    f(&DockManager::from_glib_borrow(this).unsafe_cast(), &from_glib_borrow(object))
}

impl fmt::Display for DockManager {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DockManager")
    }
}
