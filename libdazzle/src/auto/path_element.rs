// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use dazzle_sys;
#[cfg(any(feature = "v3_26", feature = "dox"))]
use glib::GString;
use glib::object::IsA;
use glib::translate::*;
use std::fmt;

glib_wrapper! {
    pub struct PathElement(Object<dazzle_sys::DzlPathElement, dazzle_sys::DzlPathElementClass, PathElementClass>);

    match fn {
        get_type => || dazzle_sys::dzl_path_element_get_type(),
    }
}

impl PathElement {
    /// Creates a new path element for an `Path`.
    ///
    /// Feature: `v3_26`
    ///
    /// ## `id`
    /// An id for the path element.
    /// ## `icon_name`
    /// An optional icon name for the element
    /// ## `title`
    /// The title of the element.
    ///
    /// # Returns
    ///
    /// A `PathElement`
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    pub fn new(id: Option<&str>, icon_name: Option<&str>, title: &str) -> PathElement {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(dazzle_sys::dzl_path_element_new(id.to_glib_none().0, icon_name.to_glib_none().0, title.to_glib_none().0))
        }
    }
}

pub const NONE_PATH_ELEMENT: Option<&PathElement> = None;

/// Trait containing all `PathElement` methods.
///
/// Feature: `v3_26`
///
/// # Implementors
///
/// [`PathElement`](struct.PathElement.html)
pub trait PathElementExt: 'static {
    /// Gets the `PathElement:icon-name` property. This is used by the
    /// path bar to display an icon next to the element of the path.
    ///
    /// Feature: `v3_26`
    ///
    ///
    /// # Returns
    ///
    /// The icon-name for the `PathElement`.
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn get_icon_name(&self) -> Option<GString>;

    /// Gets the id for the element. Generally, a path is built of
    /// multiple elements and each element should have an id that
    /// is useful to the application that it using it. You might store
    /// the name of a directory, or some other key as the id.
    ///
    /// Feature: `v3_26`
    ///
    ///
    /// # Returns
    ///
    /// The id for the `PathElement`.
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn get_id(&self) -> Option<GString>;

    /// Gets the `PathElement:title` property. This is used by the
    /// path bar to display text representing the element of the path.
    ///
    /// Feature: `v3_26`
    ///
    ///
    /// # Returns
    ///
    /// The title for the `PathElement`.
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn get_title(&self) -> Option<GString>;
}

impl<O: IsA<PathElement>> PathElementExt for O {
    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn get_icon_name(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_path_element_get_icon_name(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn get_id(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_path_element_get_id(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v3_26", feature = "dox"))]
    fn get_title(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_path_element_get_title(self.as_ref().to_glib_none().0))
        }
    }
}

impl fmt::Display for PathElement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "PathElement")
    }
}
