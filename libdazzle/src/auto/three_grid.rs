// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use dazzle_sys;
use glib::StaticType;
use glib::Value;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect_raw;
use glib::translate::*;
use glib_sys;
use gobject_sys;
use gtk;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib_wrapper! {
    pub struct ThreeGrid(Object<dazzle_sys::DzlThreeGrid, dazzle_sys::DzlThreeGridClass, ThreeGridClass>) @extends gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        get_type => || dazzle_sys::dzl_three_grid_get_type(),
    }
}

impl ThreeGrid {
    pub fn new() -> ThreeGrid {
        assert_initialized_main_thread!();
        unsafe {
            gtk::Widget::from_glib_none(dazzle_sys::dzl_three_grid_new()).unsafe_cast()
        }
    }
}

impl Default for ThreeGrid {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_THREE_GRID: Option<&ThreeGrid> = None;

/// Trait containing all `ThreeGrid` methods.
///
/// # Implementors
///
/// [`ThreeGrid`](struct.ThreeGrid.html)
pub trait ThreeGridExt: 'static {
    fn get_property_column_spacing(&self) -> u32;

    fn set_property_column_spacing(&self, column_spacing: u32);

    fn get_property_row_spacing(&self) -> u32;

    fn set_property_row_spacing(&self, row_spacing: u32);

    fn connect_property_column_spacing_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_row_spacing_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<ThreeGrid>> ThreeGridExt for O {
    fn get_property_column_spacing(&self) -> u32 {
        unsafe {
            let mut value = Value::from_type(<u32 as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"column-spacing\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get().unwrap()
        }
    }

    fn set_property_column_spacing(&self, column_spacing: u32) {
        unsafe {
            gobject_sys::g_object_set_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"column-spacing\0".as_ptr() as *const _, Value::from(&column_spacing).to_glib_none().0);
        }
    }

    fn get_property_row_spacing(&self) -> u32 {
        unsafe {
            let mut value = Value::from_type(<u32 as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"row-spacing\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get().unwrap()
        }
    }

    fn set_property_row_spacing(&self, row_spacing: u32) {
        unsafe {
            gobject_sys::g_object_set_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"row-spacing\0".as_ptr() as *const _, Value::from(&row_spacing).to_glib_none().0);
        }
    }

    fn connect_property_column_spacing_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::column-spacing\0".as_ptr() as *const _,
                Some(transmute(notify_column_spacing_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_row_spacing_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::row-spacing\0".as_ptr() as *const _,
                Some(transmute(notify_row_spacing_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }
}

unsafe extern "C" fn notify_column_spacing_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlThreeGrid, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<ThreeGrid> {
    let f: &F = &*(f as *const F);
    f(&ThreeGrid::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_row_spacing_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlThreeGrid, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<ThreeGrid> {
    let f: &F = &*(f as *const F);
    f(&ThreeGrid::from_glib_borrow(this).unsafe_cast())
}

impl fmt::Display for ThreeGrid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ThreeGrid")
    }
}
