// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use dazzle_sys;
use glib::GString;
use glib::StaticType;
use glib::Value;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect_raw;
use glib::translate::*;
use glib_sys;
use gobject_sys;
use gtk;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib_wrapper! {
    pub struct RadioBox(Object<dazzle_sys::DzlRadioBox, dazzle_sys::DzlRadioBoxClass, RadioBoxClass>) @extends gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        get_type => || dazzle_sys::dzl_radio_box_get_type(),
    }
}

impl RadioBox {
    pub fn new() -> RadioBox {
        assert_initialized_main_thread!();
        unsafe {
            gtk::Widget::from_glib_none(dazzle_sys::dzl_radio_box_new()).unsafe_cast()
        }
    }
}

impl Default for RadioBox {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_RADIO_BOX: Option<&RadioBox> = None;

/// Trait containing all `RadioBox` methods.
///
/// # Implementors
///
/// [`RadioBox`](struct.RadioBox.html)
pub trait RadioBoxExt: 'static {
    fn add_item(&self, id: &str, text: &str);

    fn get_active_id(&self) -> Option<GString>;

    fn remove_item(&self, id: &str);

    fn set_active_id(&self, id: &str);

    fn get_property_has_more(&self) -> bool;

    fn get_property_show_more(&self) -> bool;

    fn set_property_show_more(&self, show_more: bool);

    fn connect_changed<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_active_id_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_has_more_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_show_more_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<RadioBox>> RadioBoxExt for O {
    fn add_item(&self, id: &str, text: &str) {
        unsafe {
            dazzle_sys::dzl_radio_box_add_item(self.as_ref().to_glib_none().0, id.to_glib_none().0, text.to_glib_none().0);
        }
    }

    fn get_active_id(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_radio_box_get_active_id(self.as_ref().to_glib_none().0))
        }
    }

    fn remove_item(&self, id: &str) {
        unsafe {
            dazzle_sys::dzl_radio_box_remove_item(self.as_ref().to_glib_none().0, id.to_glib_none().0);
        }
    }

    fn set_active_id(&self, id: &str) {
        unsafe {
            dazzle_sys::dzl_radio_box_set_active_id(self.as_ref().to_glib_none().0, id.to_glib_none().0);
        }
    }

    fn get_property_has_more(&self) -> bool {
        unsafe {
            let mut value = Value::from_type(<bool as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"has-more\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get().unwrap()
        }
    }

    fn get_property_show_more(&self) -> bool {
        unsafe {
            let mut value = Value::from_type(<bool as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"show-more\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get().unwrap()
        }
    }

    fn set_property_show_more(&self, show_more: bool) {
        unsafe {
            gobject_sys::g_object_set_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"show-more\0".as_ptr() as *const _, Value::from(&show_more).to_glib_none().0);
        }
    }

    fn connect_changed<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"changed\0".as_ptr() as *const _,
                Some(transmute(changed_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_active_id_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::active-id\0".as_ptr() as *const _,
                Some(transmute(notify_active_id_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_has_more_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::has-more\0".as_ptr() as *const _,
                Some(transmute(notify_has_more_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_show_more_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::show-more\0".as_ptr() as *const _,
                Some(transmute(notify_show_more_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }
}

unsafe extern "C" fn changed_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlRadioBox, f: glib_sys::gpointer)
where P: IsA<RadioBox> {
    let f: &F = &*(f as *const F);
    f(&RadioBox::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_active_id_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlRadioBox, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<RadioBox> {
    let f: &F = &*(f as *const F);
    f(&RadioBox::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_has_more_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlRadioBox, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<RadioBox> {
    let f: &F = &*(f as *const F);
    f(&RadioBox::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_show_more_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlRadioBox, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<RadioBox> {
    let f: &F = &*(f as *const F);
    f(&RadioBox::from_glib_borrow(this).unsafe_cast())
}

impl fmt::Display for RadioBox {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "RadioBox")
    }
}
