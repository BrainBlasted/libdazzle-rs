// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

use Suggestion;
use dazzle_sys;
use gio;
use glib::StaticType;
use glib::Value;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect_raw;
use glib::translate::*;
use glib_sys;
use gobject_sys;
use gtk;
use pango;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib_wrapper! {
    pub struct SuggestionPopover(Object<dazzle_sys::DzlSuggestionPopover, dazzle_sys::DzlSuggestionPopoverClass, SuggestionPopoverClass>) @extends gtk::Window, gtk::Bin, gtk::Container, gtk::Widget, @implements gtk::Buildable;

    match fn {
        get_type => || dazzle_sys::dzl_suggestion_popover_get_type(),
    }
}

impl SuggestionPopover {
    pub fn new() -> SuggestionPopover {
        assert_initialized_main_thread!();
        unsafe {
            gtk::Widget::from_glib_none(dazzle_sys::dzl_suggestion_popover_new()).unsafe_cast()
        }
    }
}

impl Default for SuggestionPopover {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_SUGGESTION_POPOVER: Option<&SuggestionPopover> = None;

/// Trait containing all `SuggestionPopover` methods.
///
/// # Implementors
///
/// [`SuggestionPopover`](struct.SuggestionPopover.html)
pub trait SuggestionPopoverExt: 'static {
    fn activate_selected(&self);

    /// Gets the model being visualized.
    ///
    /// # Returns
    ///
    /// A `gio::ListModel` or `None`.
    fn get_model(&self) -> Option<gio::ListModel>;

    ///
    /// # Returns
    ///
    /// A `gtk::Widget` or `None`.
    fn get_relative_to(&self) -> Option<gtk::Widget>;

    /// Gets the currently selected suggestion.
    ///
    /// # Returns
    ///
    /// An `Suggestion` or `None`.
    fn get_selected(&self) -> Option<Suggestion>;

    fn move_by(&self, amount: i32);

    fn popdown(&self);

    fn popup(&self);

    fn set_model<P: IsA<gio::ListModel>>(&self, model: &P);

    fn set_relative_to<P: IsA<gtk::Widget>>(&self, widget: &P);

    fn set_selected<P: IsA<Suggestion>>(&self, suggestion: &P);

    fn get_property_subtitle_ellipsize(&self) -> pango::EllipsizeMode;

    fn set_property_subtitle_ellipsize(&self, subtitle_ellipsize: pango::EllipsizeMode);

    fn get_property_title_ellipsize(&self) -> pango::EllipsizeMode;

    fn set_property_title_ellipsize(&self, title_ellipsize: pango::EllipsizeMode);

    fn connect_suggestion_activated<F: Fn(&Self, &Suggestion) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_model_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_relative_to_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_selected_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_subtitle_ellipsize_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_title_ellipsize_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<SuggestionPopover>> SuggestionPopoverExt for O {
    fn activate_selected(&self) {
        unsafe {
            dazzle_sys::dzl_suggestion_popover_activate_selected(self.as_ref().to_glib_none().0);
        }
    }

    fn get_model(&self) -> Option<gio::ListModel> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_suggestion_popover_get_model(self.as_ref().to_glib_none().0))
        }
    }

    fn get_relative_to(&self) -> Option<gtk::Widget> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_suggestion_popover_get_relative_to(self.as_ref().to_glib_none().0))
        }
    }

    fn get_selected(&self) -> Option<Suggestion> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_suggestion_popover_get_selected(self.as_ref().to_glib_none().0))
        }
    }

    fn move_by(&self, amount: i32) {
        unsafe {
            dazzle_sys::dzl_suggestion_popover_move_by(self.as_ref().to_glib_none().0, amount);
        }
    }

    fn popdown(&self) {
        unsafe {
            dazzle_sys::dzl_suggestion_popover_popdown(self.as_ref().to_glib_none().0);
        }
    }

    fn popup(&self) {
        unsafe {
            dazzle_sys::dzl_suggestion_popover_popup(self.as_ref().to_glib_none().0);
        }
    }

    fn set_model<P: IsA<gio::ListModel>>(&self, model: &P) {
        unsafe {
            dazzle_sys::dzl_suggestion_popover_set_model(self.as_ref().to_glib_none().0, model.as_ref().to_glib_none().0);
        }
    }

    fn set_relative_to<P: IsA<gtk::Widget>>(&self, widget: &P) {
        unsafe {
            dazzle_sys::dzl_suggestion_popover_set_relative_to(self.as_ref().to_glib_none().0, widget.as_ref().to_glib_none().0);
        }
    }

    fn set_selected<P: IsA<Suggestion>>(&self, suggestion: &P) {
        unsafe {
            dazzle_sys::dzl_suggestion_popover_set_selected(self.as_ref().to_glib_none().0, suggestion.as_ref().to_glib_none().0);
        }
    }

    fn get_property_subtitle_ellipsize(&self) -> pango::EllipsizeMode {
        unsafe {
            let mut value = Value::from_type(<pango::EllipsizeMode as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"subtitle-ellipsize\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get().unwrap()
        }
    }

    fn set_property_subtitle_ellipsize(&self, subtitle_ellipsize: pango::EllipsizeMode) {
        unsafe {
            gobject_sys::g_object_set_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"subtitle-ellipsize\0".as_ptr() as *const _, Value::from(&subtitle_ellipsize).to_glib_none().0);
        }
    }

    fn get_property_title_ellipsize(&self) -> pango::EllipsizeMode {
        unsafe {
            let mut value = Value::from_type(<pango::EllipsizeMode as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"title-ellipsize\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get().unwrap()
        }
    }

    fn set_property_title_ellipsize(&self, title_ellipsize: pango::EllipsizeMode) {
        unsafe {
            gobject_sys::g_object_set_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"title-ellipsize\0".as_ptr() as *const _, Value::from(&title_ellipsize).to_glib_none().0);
        }
    }

    fn connect_suggestion_activated<F: Fn(&Self, &Suggestion) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"suggestion-activated\0".as_ptr() as *const _,
                Some(transmute(suggestion_activated_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_model_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::model\0".as_ptr() as *const _,
                Some(transmute(notify_model_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_relative_to_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::relative-to\0".as_ptr() as *const _,
                Some(transmute(notify_relative_to_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_selected_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::selected\0".as_ptr() as *const _,
                Some(transmute(notify_selected_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_subtitle_ellipsize_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::subtitle-ellipsize\0".as_ptr() as *const _,
                Some(transmute(notify_subtitle_ellipsize_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_title_ellipsize_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::title-ellipsize\0".as_ptr() as *const _,
                Some(transmute(notify_title_ellipsize_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }
}

unsafe extern "C" fn suggestion_activated_trampoline<P, F: Fn(&P, &Suggestion) + 'static>(this: *mut dazzle_sys::DzlSuggestionPopover, object: *mut dazzle_sys::DzlSuggestion, f: glib_sys::gpointer)
where P: IsA<SuggestionPopover> {
    let f: &F = &*(f as *const F);
    f(&SuggestionPopover::from_glib_borrow(this).unsafe_cast(), &from_glib_borrow(object))
}

unsafe extern "C" fn notify_model_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestionPopover, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<SuggestionPopover> {
    let f: &F = &*(f as *const F);
    f(&SuggestionPopover::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_relative_to_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestionPopover, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<SuggestionPopover> {
    let f: &F = &*(f as *const F);
    f(&SuggestionPopover::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_selected_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestionPopover, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<SuggestionPopover> {
    let f: &F = &*(f as *const F);
    f(&SuggestionPopover::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_subtitle_ellipsize_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestionPopover, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<SuggestionPopover> {
    let f: &F = &*(f as *const F);
    f(&SuggestionPopover::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_title_ellipsize_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestionPopover, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<SuggestionPopover> {
    let f: &F = &*(f as *const F);
    f(&SuggestionPopover::from_glib_borrow(this).unsafe_cast())
}

impl fmt::Display for SuggestionPopover {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SuggestionPopover")
    }
}
