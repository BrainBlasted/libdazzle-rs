// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

#[cfg(any(feature = "v3_30", feature = "dox"))]
use cairo;
use dazzle_sys;
use gio;
use glib::GString;
use glib::StaticType;
use glib::Value;
use glib::object::Cast;
use glib::object::IsA;
use glib::signal::SignalHandlerId;
use glib::signal::connect_raw;
use glib::translate::*;
use glib_sys;
use gobject_sys;
#[cfg(any(feature = "v3_30", feature = "dox"))]
use gtk;
use libc;
use std::boxed::Box as Box_;
use std::fmt;
use std::mem::transmute;

glib_wrapper! {
    pub struct Suggestion(Object<dazzle_sys::DzlSuggestion, dazzle_sys::DzlSuggestionClass, SuggestionClass>);

    match fn {
        get_type => || dazzle_sys::dzl_suggestion_get_type(),
    }
}

impl Suggestion {
    pub fn new() -> Suggestion {
        assert_initialized_main_thread!();
        unsafe {
            from_glib_full(dazzle_sys::dzl_suggestion_new())
        }
    }
}

impl Default for Suggestion {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_SUGGESTION: Option<&Suggestion> = None;

/// Trait containing all `Suggestion` methods.
///
/// # Implementors
///
/// [`Suggestion`](struct.Suggestion.html)
pub trait SuggestionExt: 'static {
    /// Gets the icon for the suggestion, if any.
    ///
    /// Feature: `v3_30`
    ///
    ///
    /// # Returns
    ///
    /// a `gio::Icon` or `None`
    #[cfg(any(feature = "v3_30", feature = "dox"))]
    fn get_icon(&self) -> Option<gio::Icon>;

    fn get_icon_name(&self) -> Option<GString>;

    /// This function allows subclasses to dynamicly generate content for the
    /// suggestion such as may be required when integrating with favicons or
    /// similar.
    ///
    /// `widget` is provided so that the implementation may determine scale or
    /// any other style-specific settings from the style context.
    ///
    /// Feature: `v3_30`
    ///
    /// ## `widget`
    /// a widget that may contain the surface
    ///
    /// # Returns
    ///
    /// a `cairo::Surface` or `None`
    #[cfg(any(feature = "v3_30", feature = "dox"))]
    fn get_icon_surface<P: IsA<gtk::Widget>>(&self, widget: &P) -> Option<cairo::Surface>;

    fn get_id(&self) -> Option<GString>;

    fn get_subtitle(&self) -> Option<GString>;

    fn get_title(&self) -> Option<GString>;

    /// This function is meant to be used to replace the text in the entry with text
    /// that represents the suggestion most accurately. This happens when the user
    /// presses tab while typing a suggestion. For example, if typing "gno" in the
    /// entry, you might have a suggest_suffix of "me.org" so that the user sees
    /// "gnome.org". But the replace_typed_text might include more data such as
    /// "https://gnome.org" as it more closely represents the suggestion.
    /// ## `typed_text`
    /// the text that was typed into the entry
    ///
    /// # Returns
    ///
    /// The replacement text to insert into
    ///  the entry when "tab" is pressed to complete the insertion.
    fn replace_typed_text(&self, typed_text: &str) -> Option<GString>;

    fn set_icon_name(&self, icon_name: &str);

    fn set_id(&self, id: &str);

    fn set_subtitle(&self, subtitle: &str);

    fn set_title(&self, title: &str);

    /// This function requests potential text to append to `typed_text` to make it
    /// more clear to the user what they will be activating by selecting this
    /// suggestion. For example, if they start typing "gno", a potential suggested
    /// suffix might be "me.org" to create "gnome.org".
    /// ## `typed_text`
    /// The user entered text
    ///
    /// # Returns
    ///
    /// Suffix to append to `typed_text`
    ///  or `None` to leave it unchanged.
    fn suggest_suffix(&self, typed_text: &str) -> Option<GString>;

    fn get_property_icon(&self) -> Option<gio::Icon>;

    fn connect_replace_typed_text<F: Fn(&Self, &str) -> String + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_suggest_suffix<F: Fn(&Self, &str) -> String + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_icon_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_icon_name_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_id_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_subtitle_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;

    fn connect_property_title_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId;
}

impl<O: IsA<Suggestion>> SuggestionExt for O {
    #[cfg(any(feature = "v3_30", feature = "dox"))]
    fn get_icon(&self) -> Option<gio::Icon> {
        unsafe {
            from_glib_full(dazzle_sys::dzl_suggestion_get_icon(self.as_ref().to_glib_none().0))
        }
    }

    fn get_icon_name(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_suggestion_get_icon_name(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v3_30", feature = "dox"))]
    fn get_icon_surface<P: IsA<gtk::Widget>>(&self, widget: &P) -> Option<cairo::Surface> {
        unsafe {
            from_glib_full(dazzle_sys::dzl_suggestion_get_icon_surface(self.as_ref().to_glib_none().0, widget.as_ref().to_glib_none().0))
        }
    }

    fn get_id(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_suggestion_get_id(self.as_ref().to_glib_none().0))
        }
    }

    fn get_subtitle(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_suggestion_get_subtitle(self.as_ref().to_glib_none().0))
        }
    }

    fn get_title(&self) -> Option<GString> {
        unsafe {
            from_glib_none(dazzle_sys::dzl_suggestion_get_title(self.as_ref().to_glib_none().0))
        }
    }

    fn replace_typed_text(&self, typed_text: &str) -> Option<GString> {
        unsafe {
            from_glib_full(dazzle_sys::dzl_suggestion_replace_typed_text(self.as_ref().to_glib_none().0, typed_text.to_glib_none().0))
        }
    }

    fn set_icon_name(&self, icon_name: &str) {
        unsafe {
            dazzle_sys::dzl_suggestion_set_icon_name(self.as_ref().to_glib_none().0, icon_name.to_glib_none().0);
        }
    }

    fn set_id(&self, id: &str) {
        unsafe {
            dazzle_sys::dzl_suggestion_set_id(self.as_ref().to_glib_none().0, id.to_glib_none().0);
        }
    }

    fn set_subtitle(&self, subtitle: &str) {
        unsafe {
            dazzle_sys::dzl_suggestion_set_subtitle(self.as_ref().to_glib_none().0, subtitle.to_glib_none().0);
        }
    }

    fn set_title(&self, title: &str) {
        unsafe {
            dazzle_sys::dzl_suggestion_set_title(self.as_ref().to_glib_none().0, title.to_glib_none().0);
        }
    }

    fn suggest_suffix(&self, typed_text: &str) -> Option<GString> {
        unsafe {
            from_glib_full(dazzle_sys::dzl_suggestion_suggest_suffix(self.as_ref().to_glib_none().0, typed_text.to_glib_none().0))
        }
    }

    fn get_property_icon(&self) -> Option<gio::Icon> {
        unsafe {
            let mut value = Value::from_type(<gio::Icon as StaticType>::static_type());
            gobject_sys::g_object_get_property(self.to_glib_none().0 as *mut gobject_sys::GObject, b"icon\0".as_ptr() as *const _, value.to_glib_none_mut().0);
            value.get()
        }
    }

    fn connect_replace_typed_text<F: Fn(&Self, &str) -> String + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"replace-typed-text\0".as_ptr() as *const _,
                Some(transmute(replace_typed_text_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_suggest_suffix<F: Fn(&Self, &str) -> String + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"suggest-suffix\0".as_ptr() as *const _,
                Some(transmute(suggest_suffix_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_icon_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::icon\0".as_ptr() as *const _,
                Some(transmute(notify_icon_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_icon_name_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::icon-name\0".as_ptr() as *const _,
                Some(transmute(notify_icon_name_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_id_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::id\0".as_ptr() as *const _,
                Some(transmute(notify_id_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_subtitle_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::subtitle\0".as_ptr() as *const _,
                Some(transmute(notify_subtitle_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }

    fn connect_property_title_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(self.as_ptr() as *mut _, b"notify::title\0".as_ptr() as *const _,
                Some(transmute(notify_title_trampoline::<Self, F> as usize)), Box_::into_raw(f))
        }
    }
}

unsafe extern "C" fn replace_typed_text_trampoline<P, F: Fn(&P, &str) -> String + 'static>(this: *mut dazzle_sys::DzlSuggestion, object: *mut libc::c_char, f: glib_sys::gpointer) -> *mut libc::c_char
where P: IsA<Suggestion> {
    let f: &F = &*(f as *const F);
    f(&Suggestion::from_glib_borrow(this).unsafe_cast(), &GString::from_glib_borrow(object)).to_glib_full()
}

unsafe extern "C" fn suggest_suffix_trampoline<P, F: Fn(&P, &str) -> String + 'static>(this: *mut dazzle_sys::DzlSuggestion, object: *mut libc::c_char, f: glib_sys::gpointer) -> *mut libc::c_char
where P: IsA<Suggestion> {
    let f: &F = &*(f as *const F);
    f(&Suggestion::from_glib_borrow(this).unsafe_cast(), &GString::from_glib_borrow(object)).to_glib_full()
}

unsafe extern "C" fn notify_icon_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestion, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<Suggestion> {
    let f: &F = &*(f as *const F);
    f(&Suggestion::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_icon_name_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestion, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<Suggestion> {
    let f: &F = &*(f as *const F);
    f(&Suggestion::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_id_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestion, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<Suggestion> {
    let f: &F = &*(f as *const F);
    f(&Suggestion::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_subtitle_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestion, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<Suggestion> {
    let f: &F = &*(f as *const F);
    f(&Suggestion::from_glib_borrow(this).unsafe_cast())
}

unsafe extern "C" fn notify_title_trampoline<P, F: Fn(&P) + 'static>(this: *mut dazzle_sys::DzlSuggestion, _param_spec: glib_sys::gpointer, f: glib_sys::gpointer)
where P: IsA<Suggestion> {
    let f: &F = &*(f as *const F);
    f(&Suggestion::from_glib_borrow(this).unsafe_cast())
}

impl fmt::Display for Suggestion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Suggestion")
    }
}
