// lib.rs
//
// Copyright 2019 Christopher Davis <brainblasted@disroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(deprecated)]
#![allow(dead_code)]

extern crate cairo;
extern crate cairo_sys;
#[macro_use]
extern crate bitflags;
extern crate libdazzle_sys as dazzle_sys;
extern crate gdk;
extern crate gdk_sys;
extern crate gio;
extern crate gio_sys;
#[macro_use]
extern crate glib;
extern crate glib_sys;
extern crate gobject_sys;
extern crate gtk;
extern crate gtk_sys;
extern crate libc;
extern crate pango;
extern crate pango_sys;

macro_rules! assert_initialized_main_thread {
    () => {
        if !::gtk::is_initialized_main_thread() {
            if ::gtk::is_initialized() {
                panic!("Libdazzle may only be used from the main thread.");
            } else {
                panic!("Gtk has to be initialized before using libdazzle.");
            }
        }
    };
}

macro_rules! skip_assert_initialized {
    () => {};
}

pub use glib::Error;
pub mod prelude;
pub use prelude::*;
#[cfg_attr(feature = "cargo-clippy", allow(too_many_arguments))]
#[cfg_attr(feature = "cargo-clippy", allow(useless_transmute))]
mod auto;
pub use auto::*;
